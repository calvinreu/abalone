package internal

//Action contains arguments required to make a move
type Action struct {
	player uint8
}

//StraightEmpty is a straight move to an empty field
func StraightEmpty() {

}
