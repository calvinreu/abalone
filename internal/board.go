package internal

const (
	empty = 1
	black = 2
	white = 3
)

var r0 = [...]uint8{black, black, black, black, black}
var r1 = [...]uint8{black, black, black, black, black, black}
var r2 = [...]uint8{empty, empty, black, black, black, empty, empty}
var r3 = [...]uint8{empty, empty, empty, empty, empty, empty, empty, empty}
var r4 = [...]uint8{empty, empty, empty, empty, empty, empty, empty, empty, empty}
var r5 = [...]uint8{empty, empty, empty, empty, empty, empty, empty, empty}
var r6 = [...]uint8{empty, empty, white, white, white, empty, empty}
var r7 = [...]uint8{white, white, white, white, white, white}
var r8 = [...]uint8{white, white, white, white, white}

//BoardInitData is the init Value for Board
var BoardInitData = [...]*uint8{&r0[0], &r1[0], &r2[0], &r3[0], &r4[0], &r5[0], &r6[0], &r7[0], &r8[0]}

//Board is a class which should be initialized with BoardInitData and contains non constant board info
type Board struct {
	BoardData [9]*uint8
}

//NewBoard inits board with BoardInitData
func NewBoard() Board {
	var board Board
	board.BoardData = BoardInitData
	return board
}
