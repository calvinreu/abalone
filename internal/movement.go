package internal

const (
	right     uint8 = 1 //x++
	left      uint8 = 2 //x--
	upRight   uint8 = 3
	upLeft    uint8 = 4
	downRight uint8 = 5
	downLeft  uint8 = 6
)

//Position represents x and y value on the board
type Position struct {
	x uint8
	y uint8
}

//OppositeDirection reverses direction
func OppositeDirection(direction uint8) uint8 {

	if direction == null {
		return null
	}

	if direction < 3 {
		if direction == right {
			return left
		} else {
			return right
		}
	} else if direction < 5 {
		if direction == upRight {
			return upLeft
		} else {
			return upRight
		}
	} else {
		if direction == downRight {
			return downLeft
		} else {
			return downRight
		}
	}
}

//Move change position in direction with the value ammount
func Move(position *Position, direction uint8, ammount uint8) {

	if direction == null { //only for testing purpose
		return
	}

	if direction < 3 { //side movement
		if direction == right {
			position.x += ammount
		} else {
			position.x -= ammount
		}
	} else if direction < 5 { //up movement
		if direction == upRight {
			for position.y < position.y-ammount {
				position.y--
				if position.y > middleLayer {
					position.x++
				}
			}
		} else {
			for position.y < position.y-ammount {
				position.y--
				if position.y <= middleLayer {
					position.x--
				}
			}
		}
	} else { //down movement
		if direction == downRight {
			for position.y < position.y+ammount {
				position.y++
				if position.y < middleLayer {
					position.x++
				}
			}
		} else {
			for position.y < position.y+ammount {
				if position.y >= middleLayer {
					position.x--
				}
			}
		}
	}

}

//GetMove returns change position in direction with the value ammount
func GetMove(position Position, direction uint8, ammount uint8) Position {
	Move(&position, direction, ammount)
	return position
}
